package com.atlassian.html.encode;

import org.junit.Test;

import java.io.IOException;
import java.io.StringWriter;

import static org.junit.Assert.assertEquals;

public class TestJavascriptEncodingWriter
{
    @Test
    public void test() throws IOException
    {
        assertEncoding("", "");
        assertEncoding("a", "a");
        assertEncoding("</script>", "\\u003c/script\\u003e");
        assertEncoding("\"'", "\\u0022\\u0027");
    }

    private void assertEncoding(String input, String expected) throws IOException
    {
        StringWriter writer = new StringWriter();
        JavascriptEncodingWriter jsWriter = new JavascriptEncodingWriter(writer);
        jsWriter.write(input);
        assertEquals(expected, writer.toString());
    }
}
